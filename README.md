# Artifacts Server Handler

This project is design to handle all my packages which can be sent by some workers after 
building packages for [Archlinux](https://archlinux.org) and [Ubuntu](https://ubuntu.com)

Written using [Actix Web](https://actix.rs/)

## Description

You can test it out by building package using `cargo arch` and sending this package using curl:

```bash 
curl -H 'artifacts_data: {"file_type": "ArchLinux", "pkg_name": "artifacts_server"}' \ 
--form file='@artifacts_server-0.0.1-1-x86_64.pkg.tar.zst' http://127.0.0.1:8080/
```

Result file you can find in [folder](./tmp/arch)

### Run 

```bash 
cargo run -r
```

