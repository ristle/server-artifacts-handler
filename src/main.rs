mod pkg_manager;
mod file_manager;
mod config;
mod repository_manager;

use std::sync::Mutex;
use actix_web::{middleware, web, App, HttpServer};
use actix_multipart::form::tempfile::TempFileConfig;
use actix_web::web::Data;
use crate::config::Config;

use crate::file_manager::save_files;
use crate::repository_manager::RepositoryUpdate;


use tokio::sync::watch::channel;
use crate::pkg_manager::ArchType;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    log::info!("creating temporary upload directory");
    std::fs::create_dir_all("./tmp")?;

    log::info!("starting HTTP server at http://localhost:8080");

    let config_json = std::fs::read_to_string("settings.json").expect("Failed to read Config file");
    let config: Config = serde_json::from_str(config_json.as_str()).expect("Failed to parse settings!");
    let config_bind = config.clone();

    let port = config_bind.port.unwrap_or(8080);
    let address = config_bind.address.unwrap_or("127.0.0.1".to_string());

    log::info!("Getting settings: \n{}", serde_json::to_string_pretty(&config).unwrap());

    let (sender, receiver) = channel::<Option<ArchType>>(None);
    let mut repository_update = RepositoryUpdate::new(&config, receiver);

    tokio::spawn( async move {
        repository_update.run().await;
    });

    let sender_data = Data::new(Mutex::new(sender));
    let _server = HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .app_data(TempFileConfig::default().directory("./tmp"))
            .app_data(Data::new(Mutex::new(config.clone())))
            .app_data(sender_data.clone())
            .service(
                web::resource("/")
                    .route(web::post().to(save_files)),
            )
    })
        .bind((address.as_str(), port))?
        .workers(2)
        .run()
        .await;

    Ok(())
}
