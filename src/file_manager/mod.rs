use std::fs::Permissions;
use std::os::unix::fs::PermissionsExt;
use std::sync::Mutex;
use tokio::sync::watch::Sender;

use actix_multipart::form::MultipartForm;
use actix_multipart::form::tempfile::TempFile;
use actix_web::{Error, HttpRequest, HttpResponse, Responder};
use actix_web::web::Data;

use crate::config::Config;
use crate::pkg_manager::{ArchType, ArtifactsData};


#[derive(Debug, MultipartForm)]
pub struct UploadForm {
    #[multipart(rename = "file")]
    file: TempFile,
}

pub async fn save_files(
    request: HttpRequest,
    MultipartForm(form): MultipartForm<UploadForm>,
    config: Data<Mutex<Config>>,
    sender: Data<Mutex<Sender<Option<ArchType>>>>
) -> Result<impl Responder, Error> {
    let config = config.lock().unwrap();
    let sender = sender.lock().unwrap();

    let artifacts_data =  request.headers().get("artifacts_data").expect("Please specify artifacts data").clone();
    let artifacts_data = serde_json::from_str::<ArtifactsData>(artifacts_data.to_str().unwrap()).unwrap_or_default();

    let f = form.file;
    let path = artifacts_data.get_full_path(&config, f.file_name.unwrap())?;
    
    match sender.send(Some(artifacts_data.file_type.clone())) {
        Ok(_) => {}
        Err(error) => {
            log::error!("{}", error);
        }
    }

    dbg!("Got Artifacts MetaData: ", artifacts_data);
    log::info!("Saving file to {path}");

    let persisted_file = f.file.persist(path).expect("Failed to save file");
    persisted_file.set_permissions(Permissions::from_mode(664)).expect("Failed to set permissions");
    persisted_file.sync_all().expect("Failed to sync file!");
    
    Ok(HttpResponse::Ok())
}
