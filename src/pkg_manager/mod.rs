use actix_web::Error;
use serde::{Deserialize, Serialize};
use crate::config::Config;

#[derive(Debug, Serialize, Deserialize, Default, Eq, Hash, PartialEq, Clone, Copy)]
pub enum ArchType{
    #[default]
    ArchLinux,
    Ubuntu,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct ArtifactsData {
    pub file_type: ArchType,
    pub pkg_name: Option<String>
}


impl ArtifactsData {
    pub fn get_full_path(&self, config: &Config, file_name: String) -> Result<String, Error> {
        match config.packages.get(&self.file_type) {
            None => Err(actix_web::error::ErrorBadRequest("Config not Properly init")),
            Some(pkg) => {
                match self.file_type {
                    ArchType::ArchLinux => {
                        std::fs::create_dir_all(pkg.to_owned())?;

                        Ok(pkg.to_owned() + "/" + file_name.as_str())
                    }
                    ArchType::Ubuntu => {
                        let pkg_name = self.pkg_name.clone().expect("Failed to unwrap pkg_name");

                        let folder_path = pkg.to_owned() +
                            "/" + pkg_name.chars().next().unwrap().to_string().as_str() +
                            "/" + pkg_name.as_str() + "/";

                        std::fs::create_dir_all(folder_path.clone())?;
                        Ok(folder_path + file_name.as_str())
                    }
                }
            }
        }
    }
}