use std::collections::HashMap;
use serde::{Deserialize, Serialize};

use crate::pkg_manager::ArchType;


#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Config {
    pub packages: HashMap<ArchType, String>,
    pub port: Option<u16>,
    pub address: Option<String>
}
