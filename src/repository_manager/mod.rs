use crate::config::Config;
use crate::pkg_manager::ArchType;

use std::process::Command;
use tokio::sync::watch::Receiver;

pub struct RepositoryUpdate {
    pub config: Config,
    pub receiver: Receiver<Option<ArchType>>
}

impl RepositoryUpdate {
    pub fn new(config: &Config, receiver: Receiver<Option<ArchType>>) -> RepositoryUpdate {
        RepositoryUpdate {
            config: config.to_owned(),
            receiver
        }
    }

    pub fn update_repo(&self, arch_type: ArchType) {
        let command = match arch_type {
            ArchType::Ubuntu => {
                Command::new("fish")
                    .arg("-c")
                    .arg("\"update_ubuntu\"")
                    .output()
                    .expect("failed to execute process")
            }
            ArchType::ArchLinux => {
               Command::new("fish")
                   .arg("./arch/update_all.sh")
                   .output()
                   .expect("failed to execute process")
            }
        };

        log::info!("Success: {:?}", command.status.success());
        if command.status.success() {
            let stdout = std::str::from_utf8(&command.stdout).unwrap_or_default();
            log::info!("Output of command: {stdout}");
        } else {
            let stderr = std::str::from_utf8(&command.stderr).unwrap_or_default();
            log::error!("Error: {stderr}");
        }
    }

    pub async fn run(&mut self) {
        loop {
            let arch_type_option = *self.receiver.borrow_and_update();

            if let Some(arch_type) = arch_type_option {
                log::info!("Got type: {:?}", arch_type.clone());
                self.update_repo(arch_type);
            }

            if self.receiver.changed().await.is_err() {
                break;
            }
        }
    }
}